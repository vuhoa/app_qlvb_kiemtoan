// import {StyleSheet, Dimensions} from 'react-native';
// export default StyleSheet.create({
// })
import {
    COLOR_GREEN,
    COLOR_BLUE,
    COLOR_GRAY,
    COLOR_WHITE
} from "../config/MyColor";
export const Styles = {
    container:{
        flex:1,
        flexDirection:'column',
    },
    bgLogin: {
        width:257,
        height:143, 
        resizeMode:'stretch'
    },
    styleButtonBg:{
        flexDirection:'column',
        justifyContent:'center',
    },
    
    textButton:{
        color:'#fff',
        textAlign:'center',
        fontSize: 12,
        // justifyContent:'center',
        // alignItems:'center',
        // height:'100%',
        // flex:1,
        paddingTop:5
    },
    title: {
        color: COLOR_GREEN,
        fontSize: 17,
        textTransform: "uppercase",
        fontWeight: "bold",
        textAlign: "center",
        
        fontFamily:'UTM_HelvetIns',
       
      },
      title1: {
        color: COLOR_BLUE,
        fontSize: 17,
        textTransform: "uppercase",
        textAlign: "center",
        fontFamily:'UTMAvo_1'
      },
};