/*
This is Splash Screen, run first time when app start
*/
import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Animated,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  Image
} from "react-native";
import { Styles } from "../config/Styles";
import {
  COLOR_GREEN,
  COLOR_BLUE,
  COLOR_GRAY,
  COLOR_WHITE,
  COLOR_DARK_BLUE,
  COLOR_BLACK
} from "../config/MyColor";
import { Hoshi } from "react-native-textinput-effects";
import Ripple from "react-native-material-ripple";
// import { TouchableOpacity } from "react-native-gesture-handler";
export default class Vanbanden extends Component<Props> {
  constructor(props) {
    super(props);
  }
  static navigationOptions = ({ navigation }) => {
    let headerTitle = "Văn bản đến";
    let headerTitleStyle = {
      textAlign: "center",
      flex: 1,
      alignSelf: "center",
      color: COLOR_WHITE,
      fontSize: 12,
      fontFamily: "UTMAvo_1",
      textTransform: "uppercase"
    };
    let headerStyle = {
      borderBottomWidth: 2,
      borderBottomColor: "#6fc3e9",
      height: 44
    };
    let headerLeft = (
      <TouchableOpacity
        onPress={() => {
          navigation.goBack();
        }}
      >
        <Image
          source={require("../images/iconmoi/39.png")}
          style={{ height: 20, width: 11, marginLeft: 15 }}
        />
      </TouchableOpacity>
    );
    let headerRight = (
      <TouchableOpacity
        onPress={() => {
          navigation.navigate("Home");
        }}
      >
         <Image
          source={require("../images/iconmoi/2.png")}
          style={{ height: 18, width: 15, marginRight: 15 }}
        />
      </TouchableOpacity>
    );
    let headerBackground = (
      <Image
        source={require("../images/bg-header.png")}
        style={{ width: "100%", height: 42 }}
      />
    );
    return {
      headerBackground,
      headerTitle,
      headerStyle,
      headerLeft,
      headerTitleStyle,
      headerRight
    };
  };

  render() {
    return (
      <ImageBackground
        source={require("../images/iconmoi/75.png")}
        style={{ width: "100%", height: "100%", resizeMode: "contain" }}
      >
        <View style={Styles.container}>
        <View style={styles.boxTop}>
            <View style={styles.boxContent}>
              <View style={styles.boxAvatar}>
              <Image source={require("../images/iconmoi/Asset21.png")} style={{height:72, width:72}}/>
              </View>
              <View>
                <View style={styles.itemTop}>
                  <View style={styles.icon}>
                    <Image
                      source={require("../images/iconmoi/iconUser.png")}
                      style={{ width: 20, height: 20 }}
                    />
                  </View>
                  <Text style={styles.textName}>Hà Thị Mỹ Dung</Text>
                </View>
                <View style={styles.itemTop}>
                  <View style={styles.icon}>
                    <Image
                      source={require("../images/iconmoi/37.png")}
                      style={{ width: 20, height: 20 }}
                    />
                  </View>
                  <Text style={styles.textChucVu}>Vụ tổ chức cán bộ</Text>
                </View>
              </View>
            </View>
          </View>
          <View style={styles.middle}>
            <View style={styles.middleTop}>
              <View style={styles.boxMiddle}>
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate("VBchuaxuly");
                  }}
                >
                  <ImageBackground
                    source={require("../images/bg-small-01.png")}
                    style={styles.dimetionsBg}
                  >
                    <View style={styles.boxMiddleTop}>
                    <Image
                        source={require("../images/iconmoi/50.png")}
                        style={[styles.imageIcon, { height: 30, width: 27 }]}
                      />
                      <Text style={styles.nameText}>VB chưa xử lý</Text>
                    </View>
                  </ImageBackground>
                </TouchableOpacity>
              </View>
              <View style={styles.boxMiddle}>
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate("Tdcv");
                  }}
                >
                  <ImageBackground
                    source={require("../images/bg-small-01.png")}
                    style={styles.dimetionsBg}
                  >
                    <View style={styles.boxMiddleTop}>
                    <Image
                        source={require("../images/iconmoi/51.png")}
                        style={[styles.imageIcon, { height: 30, width: 27 }]}
                      />
                      <Text style={styles.nameText}>VB đã xử lý</Text>
                    </View>
                  </ImageBackground>
                </TouchableOpacity>
              </View>
              <View style={styles.boxMiddle}>
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate("Tdcv");
                  }}
                >
                  <ImageBackground
                    source={require("../images/bg-small-01.png")}
                    style={styles.dimetionsBg}
                  >
                    <View style={styles.boxMiddleTop}>
                    <Image
                        source={require("../images/iconmoi/52.png")}
                        style={[styles.imageIcon, { height: 30, width: 30 }]}
                      />
                      <Text style={styles.nameText}>DS văn bản</Text>
                    </View>
                  </ImageBackground>
                </TouchableOpacity>
              </View>
            </View>

            <View style={styles.middleTop}>
              <View style={styles.boxMiddle}>
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate("Tdcv");
                  }}
                >
                  <ImageBackground
                    source={require("../images/bg-big-01.png")}
                    style={styles.dimetionsBg}
                  >
                    <View style={styles.boxMiddleTop}>
                    <Image
                        source={require("../images/iconmoi/53.png")}
                        style={[styles.imageIcon, { height: 30, width: 27 }]}
                      />
                      <Text style={styles.nameText}>VB để biết</Text>
                    </View>
                  </ImageBackground>
                </TouchableOpacity>
              </View>
              <View style={styles.boxMiddle}>
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate("Tdcv");
                  }}
                >
                  <ImageBackground
                    source={require("../images/bg-big-01.png")}
                    style={styles.dimetionsBg}
                  >
                    <View style={styles.boxMiddleTop}>
                    <Image
                        source={require("../images/iconmoi/54.png")}
                        style={[styles.imageIcon, { height: 30, width: 27 }]}
                      />
                      <Text style={styles.nameText}>Vb theo dõi</Text>
                    </View>
                  </ImageBackground>
                </TouchableOpacity>
              </View>

              <View style={styles.boxMiddle}>
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate("Tdcv");
                  }}
                >
                  <ImageBackground
                    source={require("../images/bg-small-01.png")}
                    style={styles.dimetionsBg}
                  >
                    <View style={styles.boxMiddleTop}>
                    <Image
                        source={require("../images/iconmoi/55.png")}
                        style={[styles.imageIcon, { height: 30, width: 27 }]}
                      />
                      <Text style={styles.nameText}>VBYC bản gốc</Text>
                    </View>
                  </ImageBackground>
                </TouchableOpacity>
              </View>
            </View>

            <View style={styles.middleTop}>
              <View style={styles.boxMiddle}>
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate("Tdcv");
                  }}
                >
                  <ImageBackground
                    source={require("../images/bg-big-01.png")}
                    style={styles.dimetionsBg}
                  >
                    <View style={styles.boxMiddleTop}>
                    <Image
                        source={require("../images/iconmoi/56.png")}
                        style={[styles.imageIcon, { height: 30, width: 27 }]}
                      />
                      <Text style={styles.nameText}>VB chia sẻ</Text>
                    </View>
                  </ImageBackground>
                </TouchableOpacity>
              </View>
              <View style={styles.boxMiddle}>
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate("Tdcv");
                  }}
                >
                  <ImageBackground
                    source={require("../images/bg-big-01.png")}
                    style={styles.dimetionsBg}
                  >
                    <View style={styles.boxMiddleTop}>
                    <Image
                        source={require("../images/iconmoi/57.png")}
                        style={[styles.imageIcon, { height: 30, width: 31 }]}
                      />
                      <Text style={styles.nameText}>Khác</Text>
                    </View>
                  </ImageBackground>
                </TouchableOpacity>
              </View>

              <View style={styles.boxMiddle}>
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate("Tdcv");
                  }}
                >
                  <ImageBackground
                    source={require("../images/bg-small-01.png")}
                    style={styles.dimetionsBg}
                  >
                    <View style={styles.boxMiddleTop}>
                    <Image
                        source={require("../images/iconmoi/57.png")}
                        style={[styles.imageIcon, { height: 30, width: 31 }]}
                      />
                      <Text style={styles.nameText}>Khác</Text>
                    </View>
                  </ImageBackground>
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <View style={styles.footer}>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate("Home");
              }}
            >
              <View style={styles.boxFooter}>
                <Image
                  source={require("../images/iconmoi/32.png")}
                  style={{ width: 15, height: 15 }}
                />
                <Text style={styles.textFooter}>Trang chủ</Text>
              </View>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate("QlyVBDH");
              }}
            >
              <View style={styles.boxFooter}>
                <Image
                  source={require("../images/iconmoi/33.png")}
                  style={{ width: 20, height: 15 }}
                />
                <Text style={styles.textFooter}>QLVBĐH</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate("DashBoard");
              }}
            >
              <View style={styles.boxFooter}>
                <Image
                  source={require("../images/iconmoi/34.png")}
                  style={{ width: 24, height: 15 }}
                />
                <Text
                  style={[
                    styles.textFooter,
                    { fontWeight: "bold", color: COLOR_DARK_BLUE }
                  ]}
                >
                  DashBoard
                </Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate("Home");
              }}
            >
              <View style={styles.boxFooter}>
                <Image
                  source={require("../images/iconmoi/35.png")}
                  style={{ width: 15, height: 15 }}
                />
                <Text style={styles.textFooter}>Quản trị</Text>
              </View>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate("Home");
              }}
            >
              <View style={styles.boxFooter}>
                <Image
                  source={require("../images/iconmoi/36.png")}
                  style={{ width: 15, height: 15 }}
                />
                <Text style={styles.textFooter}>DownLoad</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </ImageBackground>
    );
  }
}
const styles = StyleSheet.create({
  boxTop: {
    flex: 3.5,
    flexDirection: "column"
  },
  dimetionsBg: {
    height: 77,
    width: 81
  },
  boxAvatar: {
    flexDirection: "row",
    justifyContent: "center",
    height: 70,
    width: 70,
    borderRadius: 35,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 3 },
    shadowOpacity: 0.2,
    shadowRadius: 5,
    elevation: 3,
    marginBottom: 15
  },
  boxContent: {
    flex: 1,
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center"
    // backgroundColor:'green'
  },
  boxMiddle: {
    margin: 8,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.2,
    shadowRadius: 5,
    elevation: 3
  },
  textChucVu: {
    color: COLOR_DARK_BLUE,
    fontSize: 14,
    fontFamily: "UTMAvo_1"
  },
  icon: {
    width: 30,
    height: 30
  },
  itemTop: {
    flexDirection: "row"
    // backgroundColor:'red'
  },
  imageIcon: {
    marginBottom: 5
  },
  textName: {
    color: COLOR_DARK_BLUE,
    fontSize: 16,
    fontFamily: "UTMAvoBold_1",
    textTransform: "capitalize"
  },
  nameText: {
    color: COLOR_DARK_BLUE,
    fontSize: 10,
    fontFamily: "UTMAvo_1",
    textTransform: "uppercase"
  },
  middle: {
    flex: 6,
    flexDirection: "column",
    // backgroundColor: "#f5fcffa1",
    paddingTop: 12
    // opacity:0.4
  },
  middleTop: {
    flexDirection: "row",
    justifyContent: "center"
  },
  boxMiddleTop: {
    flex: 1,
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center"
    // paddingTop: 15
  },
  middleLeft: {
    flex: 3,
    flexDirection: "column"
  },
  itemMiddleLeft: {
    justifyContent: "flex-end",
    alignItems: "flex-end",
    marginRight: 5,
    marginBottom: 10,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.2,
    shadowRadius: 5,
    elevation: 1
  },

  itemInside: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  middleRight: {
    flex: 3,
    flexDirection: "column"
  },
  itemMiddleRight: {
    marginLeft: 5,
    marginBottom: 10,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.2,
    shadowRadius: 5,
    elevation: 1
  },
  down: {
    flex: 2,
    justifyContent: "center",
    alignItems: "center"
  },
  textFooter: {
    fontSize: 10,
    color: COLOR_BLUE
  },

  itemBottom: {
    marginHorizontal: 10
  },
  footer: {
    flex: 1,
    flexDirection: "row",
    backgroundColor: COLOR_WHITE,
    borderWidth: 1,
    borderColor: "#b3b3b3",
    alignItems: "center",
    justifyContent: "space-between",
    paddingHorizontal: 10

    // position:'absolute',
    // bottom:0,
    // height: 44
  },
  boxFooter: {
    justifyContent: "center",
    alignItems: "center"
  },
  textFooter: {
    paddingTop: 5,
    fontSize: 9,
    color: COLOR_BLACK,
    fontFamily: "MyriadPro_Regular"
  }
});
