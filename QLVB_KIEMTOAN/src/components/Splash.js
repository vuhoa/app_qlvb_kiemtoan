/*
This is Splash Screen, run first time when app start
*/
import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Animated,
  Dimensions,
  ImageBackground,
  Image
} from "react-native";
import {
  COLOR_GREEN,COLOR_BLUE
} from "../config/MyColor";
export default class Splash extends Component {
  static navigationOptions = {
    header: null
  }
  state = {
    logoOpacity: new Animated.Value(0)
  }
  async componentDidMount(){
    Animated.sequence([
      Animated.timing(this.state.logoOpacity,{
        toValue :1,
        duration: 2000
      })
    ]).start(() => {
      //End Animations
      this.props.navigation.navigate("Login")
    })
  }
  render() {
    return (
      <ImageBackground
        source={require("../images/iconmoi/64.png")}
        style={{ width: "100%", height: "100%", resizeMode:'stretch' }}
      >
        <View style={styles.container}>
            <View style={styles.top}>
                <Animated.Image source={require('../images/iconmoi/Asset61.png')} style={{opacity:this.state.logoOpacity,width:150,height:150, marginTop:20}}>

                </Animated.Image>
            </View>
            <View style={styles.down}>
                <Text style={styles.textVn}>Kiểm toán nhà nước Việt Nam</Text>
                <Text style={styles.textEn}>State Audit office Of VietNam</Text>
            </View>
        </View>
      </ImageBackground>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column"
  },
  top: {
      flex: 9,
      flexDirection:"column",
      justifyContent:'center',
      alignItems:'center'
  },
  down: {
      flex:1,
      flexDirection:'column',
      alignItems:'center'
  },
  textVn:{
      color: COLOR_GREEN,
      fontSize: 14,
      textTransform:'uppercase',
      fontWeight:'bold',
      fontFamily:'UTM_HelvetIns'
  },
  textEn:{
    color:COLOR_BLUE,
    fontSize: 12,
    textTransform:'uppercase',
    fontFamily:'UTMAvo_1'
  }
});
