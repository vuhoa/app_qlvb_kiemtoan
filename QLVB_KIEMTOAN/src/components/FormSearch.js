/*
This is Splash Screen, run first time when app start
*/
import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Animated,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  Image,
  ScrollView,
  TextInput,
  Picker
} from "react-native";
import DatePicker from "react-native-datepicker";
import { Styles } from "../config/Styles";
import {
  COLOR_GREEN,
  COLOR_BLUE,
  COLOR_GRAY,
  COLOR_WHITE,
  COLOR_DARK_BLUE,
  COLOR_LIGHT_BLACK
} from "../config/MyColor";
import Ripple from "react-native-material-ripple";

export default class FormSearch extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      status:false,
      text: "",
      ngaydenTo: "",
      ngaydenFrom: "",
      ngayVbTo: "",
      ngayVbFrom: ""
    };
  }
  componetHideAndShow(){
    this.setState({status:!this.state.status});
  }
  static navigationOptions = ({ navigation }) => {
    let headerTitle = "Văn bản chưa xử lý";
    let headerTitleStyle = {
      textAlign: "center",
      flex: 1,
      alignSelf: "center",
      color: COLOR_WHITE,
      fontSize: 12,
      fontFamily: "UTMAvo_1",
      textTransform: "uppercase"
    };
    let headerStyle = {
      borderBottomWidth: 2,
      borderBottomColor: "#6fc3e9",
      height: 44
    };
    let headerLeft = (
      <TouchableOpacity
        onPress={() => {
          navigation.goBack();
        }}
      >
        <Image
          source={require("../images/iconmoi/39.png")}
          style={{ height: 20, width: 11, marginLeft: 15 }}
        />
      </TouchableOpacity>
    );
    let headerRight = (
      <View style={styles.boxSearch}>
        <TouchableOpacity
          onPress={() => {
            navigation.navigate("Home");
          }}
        >
          <Image
            source={require("../images/iconmoi/2.png")}
            style={{ height: 18, width: 15, marginRight: 15 }}
          />
        </TouchableOpacity>
      </View>
    );
    let headerBackground = (
      <Image
        source={require("../images/bg-header.png")}
        style={{ width: "100%", height: 42 }}
      />
    );
    return {
      headerBackground,
      headerTitle,
      headerStyle,
      headerLeft,
      headerTitleStyle,
      headerRight
    };
  };

  render() {
    return (
      <ImageBackground
        source={require("../images/bg-td.png")}
        style={{ width: "100%", height: "100%", resizeMode: "contain" }}
      >
        <ScrollView>
          <View style={styles.container}>
            <View style={{ flexDirection: "row" }}>
              <Image
                source={require("../images/iconmoi/Asset20.png")}
                style={{ width: 18, height: 18 }}
              />
              <Text style={styles.textTitle}>Tìm kiếm,</Text>
              <Text
                style={[styles.textTitle, { opacity: 0.8, paddingLeft: 5 }]}
              >
                Tìm kiếm nâng cao
              </Text>
            </View>
            <View style={styles.formSearch}>
              <View>
                <View style={styles.form}>
                  <View>
                    <Text style={styles.label}>Phạm vi tìm kiếm</Text>
                    <View style={styles.boxSelect}>
                      <Picker
                        selectedValue=""
                        style={{
                          borderWidth: 1,
                          borderColor: "#e5e5e5",
                          borderRadius: 5,
                          width: 273
                        }}
                        onValueChange={(itemValue, itemIndex) => {}}
                      >
                        <Picker.Item label="" value="" />
                        <Picker.Item label="" value="" />
                      </Picker>
                    </View>
                  </View>
                  <View>
                    <Text style={styles.label}>Từ khóa</Text>
                    <TextInput
                      style={{
                        height: 35,
                        borderColor: "#b3b3b3",
                        borderWidth: 1,
                        backgroundColor: COLOR_WHITE,
                        borderRadius: 6.5,
                        width: 273
                      }}
                      onChangeText={text => this.setState({ text })}
                      value={this.state.text}
                      placeholderTextColor="#999"
                      placeholder="Tìm kiếm từ khóa"
                      placeholderStyle={{
                        fontFamily: "UTMAvo_1",
                        fontSize: 13
                      }}
                    />
                  </View>
                  {/* {renderIf(this.state.status)( */}
                    {this.state.status ? 
                  <View style={styles.formExtend}>
                    <Text style={styles.label}>Số đến</Text>
                    <View style={styles.group}>
                      <View style={styles.item}>
                        <TextInput
                          style={styles.styleInput}
                          onChangeText={text => this.setState({ text })}
                          value={this.state.text}
                          placeholderTextColor="#999"
                          placeholder="Từ số"
                          placeholderStyle={{
                            fontFamily: "UTMAvo_1",
                            fontSize: 13
                          }}
                        />
                      </View>
                      <View style={styles.item}>
                        <TextInput
                          style={[styles.styleInput, { marginRight: 0 }]}
                          onChangeText={text => this.setState({ text })}
                          value={this.state.text}
                          placeholderTextColor="#999"
                          placeholder="Đến số"
                          placeholderStyle={{
                            fontFamily: "UTMAvo_1",
                            fontSize: 13
                          }}
                        />
                      </View>
                    </View>
                    <View>
                      <Text style={styles.label}>Số văn bản</Text>
                      <View style={styles.boxSelect}>
                        <Picker
                          selectedValue="Chọn số văn bản"
                          style={{
                            borderWidth: 1,
                            borderColor: "#e5e5e5",
                            borderRadius: 5,
                            width: 273
                          }}
                          onValueChange={(itemValue, itemIndex) => {}}
                        >
                          <Picker.Item
                            label="Chọn số văn bản"
                            value="Chọn số văn bản"
                          />
                          <Picker.Item label="" value="" />
                        </Picker>
                      </View>
                    </View>
                    <View>
                      <Text style={styles.label}>Cơ quan gửi đến</Text>
                      <TextInput
                        style={{
                          height: 35,
                          borderColor: "#b3b3b3",
                          borderWidth: 1,
                          backgroundColor: COLOR_WHITE,
                          borderRadius: 6.5,
                          width: 273
                        }}
                        onChangeText={text => this.setState({ text })}
                        value={this.state.text}
                        placeholderTextColor="#999"
                        placeholder="Nhập từ khóa tìm kiếm"
                        placeholderStyle={{
                          fontFamily: "UTMAvo_1",
                          fontSize: 13
                        }}
                      />
                    </View>
                    <Text style={styles.label}>Ngày đến</Text>
                    <View style={styles.group}>
                      <View style={styles.item}>
                        <DatePicker
                          style={{ width: 134}}
                          date={this.state.ngaydenFrom}
                          mode="date"
                          format="DD-MM-YYYY"
                          confirmBtnText="Confirm"
                          cancelBtnText="Cancel"
                          showIcon={false}
                          customStyles={{
                            dateInput: {
                              marginLeft: 0,
                              borderWidth: 1,
                              borderRadius: 6.5,
                              paddingLeft: 10,
                              borderColor: "#ccc",
                              justifyContent: "center",
                              alignItems: "flex-start"
                            }
                          }}
                          onDateChange={date => {
                            this.setState({ ngaydenFrom: date });
                          }}
                        />
                      </View>
                      <View style={styles.item}>
                        <DatePicker
                          style={{ width: 134 }}
                          date={this.state.ngaydenTo}
                          mode="date"
                          format="DD-MM-YYYY"
                          confirmBtnText="Confirm"
                          cancelBtnText="Cancel"
                          showIcon={false}
                          customStyles={{
                            dateInput: {
                              marginLeft: 0,
                              borderWidth: 1,
                              borderRadius: 6.5,
                              paddingLeft: 10,
                              borderColor: "#ccc",
                              justifyContent: "center",
                              alignItems: "flex-start"
                            }
                          }}
                          onDateChange={date => {
                            this.setState({ ngaydenTo: date });
                          }}
                        />
                      </View>
                    </View>
                    <Text style={styles.label}>Ngày văn bản</Text>
                    <View style={styles.group}>
                      <View style={styles.item}>
                        <DatePicker
                          style={{ width: 134 }}
                          date={this.state.ngayVbFrom}
                          mode="date"
                          format="DD-MM-YYYY"
                          confirmBtnText="Confirm"
                          cancelBtnText="Cancel"
                          showIcon={false}
                          customStyles={{
                            dateInput: {
                              marginLeft: 0,
                              borderWidth: 1,
                              borderRadius: 6.5,
                              paddingLeft: 10,
                              borderColor: "#ccc",
                              justifyContent: "center",
                              alignItems: "flex-start"
                            }
                          }}
                          onDateChange={date => {
                            this.setState({ ngayVbFrom: date });
                          }}
                        />
                      </View>
                      <View style={styles.item}>
                        <DatePicker
                          style={{ width: 134 }}
                          date={this.state.ngayVbTo}
                          mode="date"
                          format="DD-MM-YYYY"
                          confirmBtnText="Confirm"
                          cancelBtnText="Cancel"
                          showIcon={false}
                          customStyles={{
                            dateInput: {
                              marginLeft: 0,
                              borderWidth: 1,
                              borderRadius: 6.5,
                              paddingLeft: 10,
                              borderColor: "#ccc",
                              justifyContent: "center",
                              alignItems: "flex-start"
                            }
                          }}
                          onDateChange={date => {
                            this.setState({ ngayVbTo: date });
                          }}
                        />
                      </View>
                    </View>

                    <View>
                      <Text style={styles.label}>Loại văn bản</Text>
                      <View style={styles.boxSelect}>
                        <Picker
                          selectedValue="Chọn loại văn bản"
                          style={{
                            borderWidth: 1,
                            borderColor: "#e5e5e5",
                            borderRadius: 5,
                            width: 273
                          }}
                          onValueChange={(itemValue, itemIndex) => {}}
                        >
                          <Picker.Item
                            label="Chọn số văn bản"
                            value="Chọn số văn bản"
                          />
                          <Picker.Item label="" value="" />
                        </Picker>
                      </View>
                    </View>

                    <View>
                      <Text style={styles.label}>Trạng thái văn bản</Text>
                      <View style={styles.boxSelect}>
                        <Picker
                          selectedValue="Tất cả"
                          style={{
                            borderWidth: 1,
                            borderColor: "#e5e5e5",
                            borderRadius: 5,
                            width: 273
                          }}
                          onValueChange={(itemValue, itemIndex) => {}}
                        >
                          <Picker.Item
                            label="Tất cả"
                            value="Tất cả"
                          />
                          <Picker.Item label="" value="" />
                        </Picker>
                      </View>
                    </View>
                  </View>
                    : null }

                  <View style={styles.bgButton}>
                    <Ripple
                      rippleColor="#fff"
                      onPress={() => {
                        this.props.navigation.navigate("VBchuaxulyketqua");
                      }}
                    >
                      <ImageBackground
                        source={require("../images/iconmoi/61.png")}
                        style={styles.styleButtonBg}
                        resizeMode="stretch"
                      >
                        <Text style={styles.textButton}>Tìm kiếm</Text>
                      </ImageBackground>
                    </Ripple>
                  </View>
                </View>
                <View style={styles.extend}>
                  <TouchableOpacity onPress={()=>this.componetHideAndShow()}>
                    <Image
                      source={require("../images/iconmoi/59.png")}
                      style={{ height: 30, width: 30 }}
                    />
                  </TouchableOpacity>
                 
                </View>
              </View>
            </View>
          </View>
        </ScrollView>
      </ImageBackground>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    margin: 13
  },
 
  boxTable: {
    flex: 1,
    marginTop: 10,
    backgroundColor: "#e1f8ff",
    borderRadius: 6,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.2,
    shadowRadius: 5,
    elevation: 3
  },
  textTitle: {
    fontSize: 14,
    fontFamily: "UTMAvoBold_1",
    color: COLOR_BLUE,
    textTransform: "uppercase",
    paddingLeft: 12
  },
  formSearch: {
    flex: 1,
    flexDirection: "row",
    borderColor: "#b3b3b3",
    marginTop: 10,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.2,
    shadowRadius: 5,
    elevation: 3,
    borderRadius: 5,
    justifyContent: "center",
    backgroundColor: COLOR_WHITE,
    alignItems: "center"
  },
  form: {
    flex: 1,
    flexDirection: "column"
  },
  boxSelect: {
    borderWidth: 1,
    borderColor: "#b3b3b3",
    borderRadius: 6.5,
    height: 35,
    alignItems: "center",
    justifyContent: "center"
  },
  label: {
    color: COLOR_LIGHT_BLACK,
    fontSize: 13,
    fontFamily: "UTMAvo_1",
    paddingVertical: 5
  },
  bgButton: {
    flex:1,
    flexDirection: "column",
    marginTop: 20
  },
  styleButtonBg: {
    flex:1,
    height: 43,
    flexDirection: "row",
    justifyContent: "center",
    alignItems:'center'
  },
  textButton: {
    fontSize: 14,
    color: COLOR_WHITE,
    fontFamily: "UTMAvo_1",
    textAlign: "center"
  },
  extend: {
    flexDirection: "row",
    justifyContent: "flex-end",
    marginTop: 10
  },

  styleInput: {
    height: 35,
    borderColor: "#b3b3b3",
    borderWidth: 1,
    backgroundColor: COLOR_WHITE,
    borderRadius: 6.5,
    marginRight: 5
  },
  formExtend: {
    flex: 1,
    flexDirection: "column"
  },
  group: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between"
  },
  item: {
    flex: 0.5
  }
});
