/*
This is Splash Screen, run first time when app start
*/
import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Animated,
  Dimensions,
  ImageBackground,
  Image,
  TouchableWithoutFeedback,
  Keyboard
} from "react-native";
import { Styles } from "../config/Styles";
import {
  COLOR_GREEN,
  COLOR_BLUE,
  COLOR_GRAY,
  COLOR_WHITE
} from "../config/MyColor";
import { Hoshi } from "react-native-textinput-effects";
import Ripple from "react-native-material-ripple";
import { TouchableOpacity } from "react-native-gesture-handler";
export default class Login extends Component {
  static navigationOptions = {
    header: null
  };
  state = {
    logoOpacity: new Animated.Value(0),
    typedText: "Tài khoản",
    typedPass: "Mật khẩu"
  };
  async componentDidMount() {
    Animated.sequence([
      Animated.timing(this.state.logoOpacity, {
        toValue: 1,
        duration: 2000
      })
    ]).start(() => {
      //End Animations
    });
  }
  render() {
    return (
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <ImageBackground
          source={require("../images/iconmoi/65.png")}
          style={{ width: "100%", height: "100%", resizeMode: "stretch" }}
        >
          <View style={styles.container}>
            <View style={styles.top}>
              <View style={styles.logo}>
                <Image
                  source={require("../images/iconmoi/Asset61.png")}
                  style={{ width: 100, height: 100, resizeMode: "contain" }}
                />
              </View>
              <View style={styles.textName}>
                <Text style={Styles.title}>Hệ thống hỗ trợ</Text>
                <Text style={Styles.title1}>Quản lý & Điều hành</Text>
              </View>
            </View>
            <View style={styles.middle}>
              <View style={{ flexDirection: "row" }}>
                <View style={{ flex: 8.5 }} />
                <View style={{ flex: 83 }}>
                  <ImageBackground
                    source={require("../images/iconmoi/62.png")}
                    resizeMode="stretch"
                    style={{
                      width: "100%",
                      height: "100%"
                    }}
                  >
                    <View style={styles.boxMiddle}>
                      <View style={styles.left}>
                        <View style={styles.boxInput}>
                          <Image
                            source={require("../images/iconmoi/avaUser.png")}
                            style={styles.imageStyle}
                          />
                          <Hoshi
                            style={styles.input}
                            placeholder="Tài khoản"
                            placeholderTextColor={COLOR_GRAY}
                            borderColor={COLOR_BLUE}
                            borderHeight={1}
                            labelHeight={24}
                            inputPadding={5}
                            inputStyle={{
                              color: COLOR_GRAY,
                              fontSize: 12,
                              marginBottom: -12
                            }}
                            onChangeText={text => {
                              // this.state
                              this.setState(previousState => {
                                return {
                                  typedText: text
                                };
                              });
                            }}
                          />
                        </View>
                        <View style={styles.boxInput}>
                          <Image
                            source={require("../images/iconmoi/iconPass.png")}
                            style={[
                              styles.imageStyle,
                              { width: 16, height: 18 }
                            ]}
                          />
                          <Hoshi
                            style={styles.input}
                            placeholder="Mật khẩu"
                            placeholderTextColor={COLOR_GRAY}
                            borderColor={COLOR_BLUE}
                            borderHeight={1}
                            labelHeight={24}
                            inputPadding={5}
                            inputStyle={{
                              color: COLOR_GRAY,
                              fontSize: 12,
                              marginBottom: -12
                            }}
                            onChangeText={text => {
                              this.setState(previousState => {
                                return {
                                  typedPass: text
                                };
                              });
                            }}
                          />
                        </View>
                        <View style={styles.bgButton}>
                          <Ripple
                            rippleColor="#fff"
                            onPress={() => {
                              this.props.navigation.navigate("Home");
                            }}
                          >
                            <ImageBackground
                              source={require("../images/iconmoi/61.png")}
                              resizeMode="stretch"
                              style={{ width: "100%", height: "100%" }}
                            >
                              <Text style={Styles.textButton}>Đăng nhập</Text>
                            </ImageBackground>
                          </Ripple>
                        </View>
                      </View>
                      <View style={styles.right}>
                        <View style={styles.boxRightTop}>
                          <Image
                            source={require("../images/iconmoi/mokhoa.png")}
                            resizeMode="contain"
                            style={{
                              width: 30,
                              height: 30,
                              
                            }}
                          />
                        </View>
                        <View style={styles.boxRightDown}>
                          <TouchableOpacity onPress={()=>{}}>
                            <View style={{justifyContent:'center',alignItems:'center'}}>
                            <Image
                            source={require("../images/iconmoi/Asset5.png")}
                            resizeMode="contain"
                            style={{ width: 30, height: 35}}
                          />
                            </View>
                          
                          <Text style={styles.text}>Đăng nhập bằng</Text>
                          <Text style={styles.text}>Vân tay/ Mã pin</Text>
                          </TouchableOpacity>
                          
                        </View>
                      </View>
                    </View>
                  </ImageBackground>
                </View>
                <View style={{ flex: 8.5 }} />
              </View>
            </View>
            <View style={styles.down}>
              <View style={styles.downTop}>
                <View style={styles.viewDown}>
                  <Ripple rippleColor="#fff" onPress={() => {}}>
                    <ImageBackground
                      source={require("../images/iconmoi/63.png")}
                      resizeMode="stretch"
                      style={{ width: 65, height: 65 }}
                    >
                      <View style={styles.boxDownTop}>
                        <Image
                          source={require("../images/iconmoi/Asset6.png")}
                          style={styles.iconDown}
                        />
                        <Text style={styles.textDown}>Đăng ký</Text>
                      </View>
                    </ImageBackground>
                  </Ripple>
                </View>
                <View style={styles.viewDown}>
                  <Ripple rippleColor="#fff" onPress={() => {}}>
                    <ImageBackground
                      source={require("../images/iconmoi/63.png")}
                      resizeMode="stretch"
                      style={{ width: 65, height: 65 }}
                    >
                      <View style={styles.boxDownTop}>
                        <Image
                          source={require("../images/iconmoi/Asset7.png")}
                          style={styles.iconDown}
                        />
                        <Text style={styles.textDown}>QRCode</Text>
                      </View>
                    </ImageBackground>
                  </Ripple>
                </View>
                <View style={styles.viewDown}>
                  <Ripple
                    rippleColor="#fff"
                    style={styles.boxBottom}
                    onPress={() => {}}
                  >
                    <ImageBackground
                      source={require("../images/iconmoi/63.png")}
                      resizeMode="stretch"
                      style={{ width: 65, height: 65 }}
                    >
                      <View style={styles.boxDownTop}>
                        <Image
                          source={require("../images/iconmoi/Asset8.png")}
                          style={styles.iconDown}
                        />
                        <Text style={styles.textDown}>Hỗ trợ</Text>
                      </View>
                    </ImageBackground>
                  </Ripple>
                </View>
              </View>
              <View style={styles.footer}>
                <Text style={styles.textFooter}>
                  © 2019 Bản quyền thuộc về Kiểm toán Nhà nước Việt Nam
                </Text>
              </View>
            </View>
          </View>
        </ImageBackground>
      </TouchableWithoutFeedback>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column"
  },
  top: {
    flex: 4.5,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
  },
  textName: {
    marginTop: 20
  },
  middle: {
    flex: 3.5,
    flexDirection: "column",
    alignItems: "center"
    // backgroundColor:'pink'
  },
  boxMiddle: {
    flex: 4,
    flexDirection: "row"
  },
  left: {
    flex: 2.5,
    flexDirection:'column'
  },
  right: {
    flexDirection: "column",
    flex: 1.5
  },
  boxRightTop: {
    flex: 2,
    alignItems: "flex-end",
    justifyContent: "center",
    marginRight: 15
  },
  boxRightDown: {
    flex: 4,
    alignItems: "center",
    justifyContent: "center"
  },
  text: {
    fontSize: 12,
    color: COLOR_GRAY
  },
  boxInput: {
    flexDirection: "row",
    flex:0.3,
    height: 35,
    position: "relative",
    alignItems: "center",
    // marginBottom: 5
  },
  input: {
    width: "80%",
    borderBottomWidth: 1,
    borderBottomColor: COLOR_GRAY,
    marginHorizontal: 30
  },
  imageStyle: {
    width: 16,
    height: 16,
    position: "absolute",
    top: 25,
    left: 8,
    bottom: 0,
    right: 10
  },
  // bottom
  down: {
    flex: 3.5,
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center"
  },
  downTop: {
    flex: 2,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "flex-end"
  },
  viewDown:{
    marginHorizontal:8
  },
  boxDownTop: {
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center",
    height: "100%"
  },
  textDown: {
    fontSize: 12,
    color: COLOR_BLUE
  },
  iconDown: {
    height: 21,
    width: 21,
    marginTop: 5
  },
  footer: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center"
  },
  textFooter: {
    color: COLOR_WHITE,
    textAlign: "center",
    fontSize: 10
  },
  bgButton:{
    flex:0.2,
    // backgroundColor:'red',
    flexDirection:'column',
    justifyContent:'center',
    marginLeft: 30,
    marginTop:20,
    // height:'100%'
    
},
});
