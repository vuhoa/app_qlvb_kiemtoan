/*
This is Splash Screen, run first time when app start
*/
import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Animated,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  Image
} from "react-native";
import { Styles } from "../config/Styles";
import {
  COLOR_GREEN,
  COLOR_BLUE,
  COLOR_GRAY,
  COLOR_WHITE
} from "../config/MyColor";
import { Hoshi } from "react-native-textinput-effects";
import Ripple from "react-native-material-ripple";
// import { TouchableOpacity } from "react-native-gesture-handler";
export default class Home extends Component<Props> {
  constructor(props){
    super(props);
  }
  static navigationOptions =({navigation})=>{
 
    // let headerStyle = { backgroundColor : 'transparent' , height : 40 };
    let headerTransparent = true;
    let headerStyle={height:40} ;
    let headerLeft =(
      <TouchableOpacity onPress={()=>
        {
          navigation.openDrawer()
        }
        }>
        <Image source ={require('../images/iconmoi/Asset19.png')} style={ { height: 15, width: 18, marginLeft: 15}}></Image>
      </TouchableOpacity>
    );
    let headerRight= (
      <TouchableOpacity onPress={()=> {navigation.navigate('Home') }}>
        <Image source ={require('../images/iconmoi/Asset20.png')} style={ { height: 20, width: 20, marginRight: 15}}></Image>
      </TouchableOpacity>
    );
    return { headerTransparent, headerStyle, headerLeft, headerRight};
  };
  render() {
    return (
      <ImageBackground
        source={require("../images/iconmoi/72-01.png")}
        style={{ width: "100%", height: "100%", resizeMode: "contain" }}
      >
        <View style={Styles.container}>
          <View style={styles.top}>
            <Text style={[Styles.title,{fontFamily:'UTM_HelvetIns'}]}>Hệ thống hỗ trợ</Text>
            <Text style={Styles.title1}>Quản lý & Điều hành</Text>
          </View>
          <View style={styles.middle}>
            <View style={styles.middleLeft}>
              <View style={styles.itemMiddleLeft}>
              <TouchableOpacity onPress={() => {this.props.navigation.navigate('DashBoard')}}>
                <ImageBackground
                  source={require("../images/bg-box-01.png")}
                  style={{ width: 122, height: 127 }}
                >
                  <View style={styles.itemInside}>
                    <Image
                      source={require("../images/iconmoi/Asset14.png")}
                      style={{ marginBottom: 15,width:41, height:26 }}
                    />
                    <Text style={styles.textName}>Dashboard</Text>
                  </View>
                </ImageBackground>
                </TouchableOpacity>
              </View>
              <View style={styles.itemMiddleLeft}>
              <TouchableOpacity onPress={() => {}}>
                <ImageBackground
                  source={require("../images/bg-box2-01.png")}
                  style={{ width: 122, height: 126 }}
                >
                  <View style={styles.itemInside}>
                    <Image
                      source={require("../images/iconmoi/17.png")}
                      style={{ marginTop: 10,width:36,height:32 }}
                    />
                    <Text style={styles.textName}>Quản lý</Text>
                    <Text style={styles.textName}>văn bản</Text>
                  </View>
                </ImageBackground>
                </TouchableOpacity>
              </View>
            </View>

            <View style={styles.middleRight}>
              <View style={styles.itemMiddleRight}>
              <TouchableOpacity onPress={() => {}}>
                <ImageBackground
                  source={require("../images/bg-box3-01.png")}
                  style={{ width: 122, height: 81 }}
                >
                  <View style={styles.itemInside}>
                    <Image source={require("../images/iconmoi/Asset15.png")} style={{width:35, height:32}}/>
                    <Text style={styles.textName}>Quản trị</Text>
                  </View>
                </ImageBackground>
                </TouchableOpacity>
              </View>

              <View style={styles.itemMiddleRight}>
              <TouchableOpacity onPress={() => {}}>
                <ImageBackground
                  source={require("../images/bg-box4-01.png")}
                  style={{ width: 122, height: 81 }}
                >
                  <View style={styles.itemInside}>
                    <Image source={require("../images/iconmoi/Asset16.png")} style={{width:31,height:35}}/>
                    <Text style={styles.textName}>DownLoad</Text>
                    <Text style={styles.textName}>tiện ích</Text>
                  </View>
                </ImageBackground>
                </TouchableOpacity>
              </View>

              <View style={styles.itemMiddleRight}>
                <TouchableOpacity onPress={() => {}}>
                  <ImageBackground
                    source={require("../images/bg-box5-01.png")}
                    style={{ width: 122, height: 81 }}
                  >
                    <View style={styles.itemInside}>
                      <Image
                        source={require("../images/iconmoi/Asset18.png")}
                        style={{ marginBottom: 10,width:26,height:29 }}
                      />
                      <Text style={styles.textName}>Thông báo</Text>
                    </View>
                  </ImageBackground>
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <View style={styles.bottom}>
            <TouchableOpacity style={styles.itemBottom} onPress={() => {}}>
              <Image source={require("../images/iconmoi/Asset9.png")} style={styles.iconDimentions}/>
            </TouchableOpacity>
            <TouchableOpacity style={styles.itemBottom} onPress={() => {}}>
              <Image source={require("../images/iconmoi/Asset10.png")} style={styles.iconDimentions}/>
            </TouchableOpacity>
            <TouchableOpacity style={styles.itemBottom} onPress={() => {}}>
              <Image source={require("../images/iconmoi/Asset11.png")} style={styles.iconDimentions}/>
            </TouchableOpacity>
            <TouchableOpacity style={styles.itemBottom} onPress={() => {}}>
              <Image source={require("../images/iconmoi/Asset12.png")} style={styles.iconDimentions}/>
            </TouchableOpacity>
            <TouchableOpacity style={styles.itemBottom} onPress={() => {}}>
              <Image source={require("../images/iconmoi/Asset13.png")} style={styles.iconDimentions}/>
            </TouchableOpacity>
          </View>
          <View style={styles.down}>
            <Text style={styles.textFooter}>
              © 2019 Bản quyền thuộc về Kiểm toán Nhà nước Việt Nam
            </Text>
          </View>
        </View>
      </ImageBackground>
    );
  }
}
const styles = StyleSheet.create({
  top: {
    flex: 2.5,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
  },
  middle: {
    flex: 4.5,
    flexDirection: "row",
    justifyContent:'center'
  },
  middleLeft: {
    flex: 3,
    flexDirection: "column"
  },
  itemMiddleLeft: {
    justifyContent: "flex-end",
    alignItems: "flex-end",
    marginRight: 5,
    marginBottom: 10,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.2,
    shadowRadius: 5,
    elevation: 3,
  },
  itemInside: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  middleRight: {
    flex: 3,
    flexDirection: "column"
  },
  itemMiddleRight: {
    marginLeft: 5,
    marginBottom: 10,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.2,
    shadowRadius: 5,
    elevation: 3
  },
  down: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    textAlign: "center"
  },
  textFooter: {
    fontSize: 10,
    color: COLOR_BLUE
  },
  textName: {
    fontSize: 13,
    color: COLOR_WHITE,
    textTransform: "uppercase"
  },
  bottom: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "flex-start",
  },
  itemBottom: {
    marginHorizontal: 10
  },
  iconDimentions:{
    width:31,
    height:31
  }
});
