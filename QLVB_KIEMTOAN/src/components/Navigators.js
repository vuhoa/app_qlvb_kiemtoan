import React, {Component} from "react";
import {
    StyleSheet, 
    View, 
    Text,
    Dimensions
} from "react-native";
import Login from './Login';
import Splash from './Splash';
import Home from './Home';
import DashBoard from './DashBoard';
import Tdcv from './Tdcv';
import Vanbanden from './Vanbanden';
import QlyVBDH from './QlyVBDH';
import VBchuaxuly from './VBchuaxuly';
import VBchuaxulyketqua from "./VBchuaxulyketqua";
import FormSearch from './FormSearch';
import SideMenu from './sideMenu';

import { createStackNavigator, createAppContainer, createDrawerNavigator, createSwitchNavigator,createBottomTabNavigator } from "react-navigation";
const AppNavigator = createStackNavigator({
    Splash: {
        screen: Splash
    },
    Login :{
        screen: Login   
    },
    Home: {
        screen: Home
    },
    DashBoard:{
        screen: DashBoard
    },
    QlyVBDH:{
        screen:QlyVBDH
    },
    Tdcv:{
        screen: Tdcv
    },
    Vanbanden:{
        screen:Vanbanden
    },
    VBchuaxuly:{
        screen: VBchuaxuly
    },
    VBchuaxulyketqua:{
        screen: VBchuaxulyketqua
    },
    FormSearch:{
        screen: FormSearch
    },


});
const SwitchNavigator = createSwitchNavigator({
    Splash: Splash,
    AppNavigator: AppNavigator
}, {
    initialRouteName: "Splash"
})
// const TabNavigator = createBottomTabNavigator({
//     Home: HomeScreen,
//     Settings: SettingsScreen,
//   });
export const MyDrawerNavigator = createDrawerNavigator({
    SwitchNavigator : {
        screen : SwitchNavigator,
    }
},
{
    drawerPosition:'left',
    drawerOpenRoute: 'DrawerOpen',
    drawerCloseRoute: 'DrawerClose',
    // drawerWidth: 201, 
    drawerWidth: Dimensions.get('window').width - 120, 
    contentComponent: props => <SideMenu {...props}/>
}
);

export default MyApp = createAppContainer(MyDrawerNavigator);