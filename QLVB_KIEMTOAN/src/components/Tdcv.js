/*
This is Splash Screen, run first time when app start
*/
import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Animated,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  Image,
  ScrollView
} from "react-native";
import { Styles } from "../config/Styles";
import {
  COLOR_GREEN,
  COLOR_BLUE,
  COLOR_GRAY,
  COLOR_WHITE,
  COLOR_DARK_BLUE,
  COLOR_LIGHT_BLACK
} from "../config/MyColor";
import { Hoshi } from "react-native-textinput-effects";
import Ripple from "react-native-material-ripple";
import Carousel from "react-native-snap-carousel";
import {
  VictoryBar,
  VictoryChart,
  VictoryTheme,
  VictoryAxis,
  VictoryPie,
} from "victory-native";

import { dataChart } from "../data/data";
const data = [{ y: 15 }, { y: 24 }, { y: 65 }, { y: 45 }, { y: 95 }];

const { width, height } = Dimensions.get("window");
const colorSwitcher: any = {
    fill: (data: any) => {
    let color = "#00a79d";
    if (data.y > 0 && data.y <= 20) {
      color = "#27aae1";
    }
    if (data.y > 20 && data.y < 25) {
      color = "#ff2e5a";
    }
    if (data.y > 25 && data.y <= 50) {
      color = "#00a79d";
    }

    if (data.y > 50 && data.y <= 75) {
      color = "#ffb100";
    }

    if (data.y > 75 && data.y <= 100) {
      color = "#5949b7";
    }

    return color;
  }
};
const SLIDER_1_FIRST_ITEM = 2;
const pageWidth = Dimensions.get("window").width;
export default class Tdcv extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      slider1ActiveSlide: SLIDER_1_FIRST_ITEM
    };
  }

  _renderItem({ item, index }) {
    return (
      <View style={styles.slide}>
        <View style={[styles.boxColorSlide,{backgroundColor:item.color}]}></View>
        <Text style={styles.textTitle}>{item.title}</Text>
        <Text style={styles.number}>{item.number}</Text>
      </View>
    );
  }

  static navigationOptions = ({ navigation }) => {
    let headerTitle = "Theo dõi công việc";
    let headerTitleStyle = {
      textAlign: "center",
      flex: 1,
      alignSelf: "center",
      color: COLOR_WHITE,
      fontSize: 12,
      fontFamily: "UTMAvo_1",
      textTransform: "uppercase"
    };
    let headerStyle = {
      borderBottomWidth: 2,
      borderBottomColor: "#6fc3e9",
      height: 44
    };
    let headerLeft = (
      <TouchableOpacity
        onPress={() => {
          navigation.goBack();
        }}
      >
        <Image
          source={require("../images/iconmoi/39.png")}
          style={{ height: 20, width: 11, marginLeft: 15 }}
        />
      </TouchableOpacity>
    );
    let headerRight = (
      <TouchableOpacity
        onPress={() => {
          navigation.goBack();
        }}
      >
        <Image
          source={require("../images/iconmoi/2.png")}
          style={{ height: 18, width: 15, marginRight: 15 }}
        />
      </TouchableOpacity>
    );
    let headerBackground = (
      <Image
        source={require("../images/bg-header.png")}
        style={{ width: "100%", height: 42 }}
      />
    );
    return {
      headerBackground,
      headerTitle,
      headerStyle,
      headerLeft,
      headerTitleStyle,
      headerRight
    };
  };

  render() {
    return (
      <ImageBackground
        source={require("../images/iconmoi/71.png")}
        style={{ width: "100%", height: "100%", resizeMode: "contain" }}
      >
        <ScrollView>
        <View style={styles.container}>
            <View style={styles.top}>
              <Image source={require("../images/iconmoi/td-01.png")} style={{height:40, width:40}}/>
              <View style={styles.boxText}>
                <Text style={styles.text}>Xử lý văn bản đến:</Text>
                <Text style={styles.text}>
                  Vụ tổ chức cán bộ - năm 2019 (24)
                </Text>
              </View>
            </View>
            <View style={styles.boxChart}>
              <View style={styles.titleChart}>
                <Image source={require("../images/iconmoi/40.png")}style={{height:14, width:13}}/>
                <Text style={[styles.textTitle,{fontFamily:'UTMAvoBold_1'}]}>Văn bản có hạn xử lý (0)</Text>
              </View>
              <View style={styles.chart}>
                <VictoryChart
                  width={width - 40}
                  height={260}
                  theme={VictoryTheme.material}
                  maxDomain={{ y: 100 }}
                  domainPadding={10}
                >
                  <VictoryAxis
                    dependentAxis
                    tickFormat={y => y}
                    orientation="left"
                  />
                  <VictoryBar
                    style={{
                      data: {
                        // fill: "#c43a31",
                        ...colorSwitcher,
                        width: 24
                      }
                    }}
                    data={data}
                    y="y"
                    alignment="start"
                  />
                </VictoryChart>
              </View>
              <View style={styles.boxNote}>
                    <Carousel 
                    rel={(c)=>this._slider1Ref = c}
                    data={dataChart}
                    renderItem={this._renderItem}
                    sliderWidth={pageWidth}
                    itemWidth={pageWidth/3}
                    loop={true}
                    autoplay={true}
                    autoplayDelay={3000}
                    autoplayInterval={3000}
                    />
              </View>
            </View>
            <View style={styles.boxChartPie}
            >
              <VictoryPie
              innerRadius={100}
              colorScale={["#00a79d", "#045ea8" ]}
              style={{ labels: { fill: "transparent" } }}
                data={[
                  {y: 2},
                  {y: 3},
                ]}
              />
              <View style={styles.ghichu}>
              <Image source={require("../images/iconmoi/40.png")} style={{height:18, width:17}}/>
                <Text style={styles.textGhichu}>VB không có thời hạn </Text>
                <Text style={styles.numberGhichu}>
                 24
                </Text>
              </View>
              <View style={styles.box}>
                  <View style={styles.ceil}>
                    <View style={styles.boxColor}></View>
                    <Text style={styles.textTitle}>Đã hoàn thành</Text>
                    <Text style={styles.number}>09</Text>
                  </View>
                  <View style={styles.ceil}>
                    <View style={styles.boxColor1}></View>
                    <Text style={styles.textTitle}>Chưa hoàn thành</Text>
                    <Text style={styles.number}>24</Text>
                  </View>
              </View>
            </View>
        </View>
        </ScrollView>
      </ImageBackground>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    padding:12
    // padding: 12
  },
  top: {
    flexDirection: "row"
  },
  boxText: {
    position: "absolute",
    left: 25
  },
  text: {
    fontSize: 11,
    textTransform: "uppercase",
    color: COLOR_BLUE,
    fontFamily: "UTMAvo_1"
  },
  titleChart: {
    flexDirection: "row",
    paddingHorizontal: 15,
    paddingTop:10
  },
  textTitle: {
    color: COLOR_LIGHT_BLACK,
    fontSize: 12,
    fontFamily: "UTMAvo_1",
    marginLeft: 10,
    textAlign:'center'
  },
  boxChart: {
    // borderWidth:1,
    borderRadius: 5,
    shadowColor: "#000",
    shadowOffset: { width: 5, height: 5 },
    shadowOpacity: 0.2,
    shadowRadius: 5,
    elevation: 4,
    backgroundColor: COLOR_WHITE,
  },
  boxChartPie:{
    marginTop:10,
    borderRadius: 5,
    shadowColor: "#000",
    shadowOffset: { width: 5, height: 5 },
    shadowOpacity: 0.2,
    shadowRadius: 5,
    elevation: 5,
    backgroundColor: COLOR_WHITE,
  },
  chart: {
    // backgroundColor: "pink"
  },
  box:{
    flexDirection:'row',
    justifyContent:'center',
    alignItems:'center'
  },
  ceil:{
    flex:5,
    borderRadius: 5,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.2,
    shadowRadius: 5,
    elevation: 4,
    backgroundColor: '#e1f8ff',
    justifyContent:'center',
    alignItems:'center',
    margin:10,
    padding:5
  },
  number:{
    color:'#ff0000',
    fontSize:20,
    fontFamily: "UTMAvo_1",
    textAlign:'center'
  },
  boxColor:{
  height:12,
  width:12,
  backgroundColor:'#045ea8',
  borderRadius:3
  },
  boxColorSlide:{
    height:12,
  width:12,
  borderRadius:3
  },
  boxColor1:{
    height:12,
    width:12,
    backgroundColor:'#00a79d',
    borderRadius:3
  },
  boxNote:{
    marginBottom:10
  },
  slide:{
    backgroundColor: '#e1f8ff',
    borderRadius: 5,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.2,
    shadowRadius: 5,
    elevation: 4,
    height:90,
    // width:108,
    flexDirection:'column',
    alignItems:'center',
    justifyContent:'center',
    padding:5
  },
  ghichu:{
    position:"absolute",
    top:'30%',
    left:'36%',
    width:120,
    flexDirection:'column',
    justifyContent:'center',
    alignItems:'center'
  },
  textGhichu:{
    textAlign:'center',
    fontFamily: "UTMAvoBold_1",
  },
  numberGhichu:{
    fontSize:28,
    color:'#224c8f',
    fontFamily: "UTMAvo_1",
    textAlign:'center'
  }
});
