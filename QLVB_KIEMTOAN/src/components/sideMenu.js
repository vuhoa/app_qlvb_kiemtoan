import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  ImageBackground,
  TouchableOpacity,
  TouchableHighlight,
  ScrollView,
  Button
} from "react-native";
import { Styles } from "../config/Styles";
import {
  COLOR_GREEN,
  COLOR_BLUE,
  COLOR_GRAY,
  COLOR_WHITE,
  COLOR_DARK_BLUE,
  COLOR_BLACK
} from "../config/MyColor";
export default class SideMenu extends Component {
  render() {
    return (
      <View style={styles.container}>
        <ImageBackground
          source={require("../images/iconmoi/73.png")}
          style={{ width: "100%", height: 87, resizeMode: "cover" }}
        >
          <View style={styles.boxInfoUser}>
            <View style={styles.boxAvatar}>
              <Image source={require("../images/iconmoi/Asset21.png")} style={{height:40,width:40}}/>
            </View>
            <View>
              <Text style={styles.textName}>Hà Thị Mỹ Dung</Text>
              <Text style={styles.textChucVu}>Vụ tổ chức cán bộ</Text>
            </View>
          </View>
        </ImageBackground>
        <ScrollView>
          <View style={styles.boxBg}>
            <View style={styles.menu}>
              {/* Thong bao */}
              <TouchableOpacity>
                <View style={styles.boxParent}>
                  <Image source={require("../images/iconmoi/Asset22.png")} style={{height:19, width:16}}/>
                  <Text style={styles.textParent}>Thông báo</Text>
                </View>
              </TouchableOpacity>

              <View style={styles.menuChild}>
                <TouchableOpacity>
                  <View style={styles.boxChild}>
                    <Image source={require("../images/iconmoi/24.png")} style={styles.iconDot}/>
                    <Text style={styles.textNoChild}>Thông báo chung</Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity>
                  <View style={styles.boxChild}>
                    <Image source={require("../images/iconmoi/24.png")} style={styles.iconDot}/>
                    <Text style={styles.textNoChild}>Thông báo văn bản đến</Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity>
                  <View style={styles.boxChild}>
                    <Image source={require("../images/iconmoi/24.png")} style={styles.iconDot}/>
                    <Text style={styles.textNoChild}>Thông báo công việc</Text>
                  </View>
                </TouchableOpacity>

                <TouchableOpacity>
                  <View style={styles.boxChild}>
                    <Image source={require("../images/iconmoi/24.png")} style={styles.iconDot}/>
                    <Text style={styles.textNoChild}>
                      Thông báo lịch làm việc
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
              {/* Quan ly vb dieu hanh */}
              <TouchableOpacity>
                <View style={styles.boxParent}>
                <Image source={require("../images/iconmoi/23.png")} style={{height:17, width:13}}/>
                  <Text style={styles.textParent}>
                    Quản lý văn bản, điều hành
                  </Text>
                </View>
              </TouchableOpacity>

              <View style={styles.menuChild}>
                <View style={styles.boxMenuChild}>
                  <TouchableOpacity>
                    <View style={styles.boxChild}>
                      <Image source={require("../images/iconmoi/24.png")} style={styles.iconDot}/>
                      <Text style={styles.textChild}>Văn bản đến</Text>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.subMenu}>
                <View style={styles.boxSubMenu}>
                  <TouchableOpacity>
                    <View style={styles.boxChild}>
                      <Text style={styles.textChildSub}>Văn bản chưa xử lý</Text>
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity>
                    <View style={styles.boxChild}>
                      <Text style={styles.textChildSub}>Văn bản đã xử lý</Text>
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity>
                    <View style={styles.boxChild}>
                      <Text style={styles.textChildSub}>Danh sách văn bản</Text>
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity>
                    <View style={styles.boxChild}>
                      <Text style={styles.textChildSub}>Văn bản để biết</Text>
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity>
                    <View style={styles.boxChild}>
                      <Text style={styles.textChildSub}>Văn bản theo dõi</Text>
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity>
                    <View style={styles.boxChild}>
                      <Text style={styles.textChildSub}>
                        Văn bản yêu cầu bản gốc
                      </Text>
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity>
                    <View style={styles.boxChild}>
                      <Text style={styles.textChildSub}>Văn bản chia sẻ</Text>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>

              {/*  VB đi */}
              <View style={styles.menuChild}>
                <View style={styles.boxMenuChild}>
                  <TouchableOpacity>
                    <View style={styles.boxChild}>
                      <Image source={require("../images/iconmoi/24.png")} style={styles.iconDot}/>
                      <Text style={styles.textChild}>Văn bản đi</Text>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.subMenu}>
                <View style={styles.boxSubMenu}>
                  <TouchableOpacity>
                    <View style={styles.boxChild}>
                      <Text style={styles.textChildSub}>Danh sách văn bản</Text>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
              {/*  */}
              <View style={styles.menuChild}>
                <View style={styles.boxMenuChild}>
                  <TouchableOpacity>
                    <View style={styles.boxChild}>
                      <Image source={require("../images/iconmoi/24.png")} style={styles.iconDot}/>
                      <Text style={styles.textChild}>Văn bản dự thảo</Text>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.subMenu}>
                <View style={styles.boxSubMenu}>
                  <TouchableOpacity>
                    <View style={styles.boxChild}>
                      <Text style={styles.textChildSub}>Thêm mới văn bản</Text>
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity>
                    <View style={styles.boxChild}>
                      <Text style={styles.textChildSub}>Danh sách VB dự thảo</Text>
                    </View>
                  </TouchableOpacity>

                  <TouchableOpacity>
                    <View style={styles.boxChild}>
                      <Text style={styles.textChildSub}>Dự thảo chưa xử lý</Text>
                    </View>
                  </TouchableOpacity>

                  <TouchableOpacity>
                    <View style={styles.boxChild}>
                      <Text style={styles.textChildSub}>Dự thảo đã xử lý</Text>
                    </View>
                  </TouchableOpacity>

                  <TouchableOpacity>
                    <View style={styles.boxChild}>
                      <Text style={styles.textChildSub}>Trao đổi nội bộ</Text>
                    </View>
                  </TouchableOpacity>

                  <TouchableOpacity>
                    <View style={styles.boxChild}>
                      <Text style={styles.textChildSub}>Ý kiến xử lý dự thảo</Text>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
              {/*  */}
              <View style={styles.menuChild}>
                <View style={styles.boxMenuChild}>
                  <TouchableOpacity>
                    <View style={styles.boxChild}>
                      <Image source={require("../images/iconmoi/24.png")} style={styles.iconDot}/>
                      <Text style={styles.textChild}>Tờ trình</Text>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.subMenu}>
                <View style={styles.boxSubMenu}>
                  <TouchableOpacity>
                    <View style={styles.boxChild}>
                      <Text style={styles.textChildSub}>Thêm mới tờ trình</Text>
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity>
                    <View style={styles.boxChild}>
                      <Text style={styles.textChildSub}>Danh sách tờ trình</Text>
                    </View>
                  </TouchableOpacity>

                  <TouchableOpacity>
                    <View style={styles.boxChild}>
                      <Text style={styles.textChildSub}>Tờ trình chưa xử lý</Text>
                    </View>
                  </TouchableOpacity>

                  <TouchableOpacity>
                    <View style={styles.boxChild}>
                      <Text style={styles.textChildSub}>Tờ trình đã xử lý</Text>
                    </View>
                  </TouchableOpacity>

                  <TouchableOpacity>
                    <View style={styles.boxChild}>
                      <Text style={styles.textChildSub}>Trao đổi nội bộ</Text>
                    </View>
                  </TouchableOpacity>

                  <TouchableOpacity>
                    <View style={styles.boxChild}>
                      <Text style={styles.textChildSub}>Ý kiến xử lý tờ trình</Text>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>

              {/*  */}
              <View style={styles.menuChild}>
                <View style={styles.boxMenuChild}>
                  <TouchableOpacity>
                    <View style={styles.boxChild}>
                      <Image source={require("../images/iconmoi/24.png")} style={styles.iconDot}/>
                      <Text style={styles.textChild}>Biểu mẫu</Text>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
              {/*  */}
             
              <View style={styles.menuChild}>
                <View style={styles.boxMenuChild}>
                  <TouchableOpacity>
                    <View style={styles.boxChild}>
                      <Image source={require("../images/iconmoi/24.png")} style={styles.iconDot}/>
                      <Text style={styles.textChild}>Công việc</Text>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.subMenu}>
                <View style={styles.boxSubMenu}>
                  <TouchableOpacity>
                    <View style={styles.boxChild}>
                      <Text style={styles.textChildSub}>Thêm mới công việc</Text>
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity>
                    <View style={styles.boxChild}>
                      <Text style={styles.textChildSub}>Danh sách công việc</Text>
                    </View>
                  </TouchableOpacity>

                  <TouchableOpacity>
                    <View style={styles.boxChild}>
                      <Text style={styles.textChildSub}>Báo cáo công việc</Text>
                    </View>
                  </TouchableOpacity>

                </View>
              </View>
              {/*  */}
              <View style={styles.menuChild}>
                <View style={styles.boxMenuChild}>
                  <TouchableOpacity>
                    <View style={styles.boxChild}>
                      <Image source={require("../images/iconmoi/24.png")} style={styles.iconDot}/>
                      <Text style={styles.textChild}>Quản lý hồ sơ</Text>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.subMenu}>
                <View style={styles.boxSubMenu}>
                  <TouchableOpacity>
                    <View style={styles.boxChild}>
                      <Text style={styles.textChildSub}>Danh sách hồ sơ</Text>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
              {/*  */}
              <View style={styles.menuChild}>
                <View style={styles.boxMenuChild}>
                  <TouchableOpacity>
                    <View style={styles.boxChild}>
                      <Image source={require("../images/iconmoi/24.png")} style={styles.iconDot}/>
                      <Text style={styles.textChild}>Báo cáo thống kê</Text>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.subMenu}>
                <View style={styles.boxSubMenu}>
                  <TouchableOpacity>
                    <View style={styles.boxChild}>
                      <Text style={styles.textChildSub}>TK văn bản đến đơn vị năm 2019</Text>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
              {/* Quan tri */}
              <TouchableOpacity>
                <View style={styles.boxParent}>
                <Image source={require("../images/iconmoi/25.png")} style={{height:15, width:15}}/>
                  <Text style={styles.textParent}>
                  Quản trị
                  </Text>
                </View>
              </TouchableOpacity>

              <View style={styles.menuChild}>
                <View style={styles.boxMenuChild}>
                  <TouchableOpacity>
                    <View style={styles.boxChild}>
                      <Image source={require("../images/iconmoi/24.png")} style={styles.iconDot}/>
                      <Text style={styles.textChild}>Tài khoản</Text>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.subMenu}>
                <View style={styles.boxSubMenu}>
                  <TouchableOpacity>
                    <View style={styles.boxChild}>
                      <Text style={styles.textChildSub}>Nhóm người dùng</Text>
                    </View>
                  </TouchableOpacity>
                  
                </View>
              </View>

              {/*  Cơ cấu tổ chức */}
              <View style={styles.menuChild}>
                <View style={styles.boxMenuChild}>
                  <TouchableOpacity>
                    <View style={styles.boxChild}>
                      <Image source={require("../images/iconmoi/24.png")} style={styles.iconDot} />
                      <Text style={styles.textChild}>Cơ cấu tổ chức</Text>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={styles.subMenu}>
                <View style={styles.boxSubMenu}>
                  <TouchableOpacity>
                    <View style={styles.boxChild}>
                      <Text style={styles.textChildSub}>Nhóm đơn vị phòng</Text>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
              {/*  */}
              <View style={styles.menuChild}>
                <View style={styles.boxMenuChild}>
                  <TouchableOpacity>
                    <View style={styles.boxChild}>
                      <Image source={require("../images/iconmoi/24.png")} style={styles.iconDot} />
                      <Text style={styles.textChild}>Log hệ thống</Text>
                    </View>
                  </TouchableOpacity>
                </View>
              </View>
              {/*  */}
              {/* DownLoad tien ich */}
              <TouchableOpacity>
                <View style={styles.boxParent}>
                <Image source={require("../images/iconmoi/26.png")} style={{height:15, width:15}}/>
                  <Text style={styles.textParent}>Download Tiện ích</Text>
                </View>
              </TouchableOpacity>

              <View style={styles.menuChild}>
                <TouchableOpacity>
                  <View style={styles.boxChild}>
                    <Image source={require("../images/iconmoi/24.png")} style={styles.iconDot} />
                    <Text style={styles.textNoChild}>Ký số bản 32 bít</Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity>
                  <View style={styles.boxChild}>
                    <Image source={require("../images/iconmoi/24.png")} style={styles.iconDot}/>
                    <Text style={styles.textNoChild}>Ký số bản 64 bít</Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity>
                  <View style={styles.boxChild}>
                    <Image source={require("../images/iconmoi/24.png")} style={styles.iconDot}/>
                    <Text style={styles.textNoChild}>Teamviewer</Text>
                  </View>
                </TouchableOpacity>

                <TouchableOpacity>
                  <View style={styles.boxChild}>
                    <Image source={require("../images/iconmoi/24.png")} style={styles.iconDot}/>
                    <Text style={styles.textNoChild}>
                    Foxit Reader
                    </Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity>
                  <View style={styles.boxChild}>
                    <Image source={require("../images/iconmoi/24.png")} style={styles.iconDot} />
                    <Text style={styles.textNoChild}>
                    DotNetFx40
                    </Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity>
                  <View style={styles.boxChild}>
                    <Image source={require("../images/iconmoi/24.png")} style={styles.iconDot} />
                    <Text style={styles.textNoChild}>
                    Hướng dẫn sửa dụng
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
              {/* Thong tin nguoi dung */}
              <TouchableOpacity>
                <View style={styles.boxParent}>
                <Image source={require("../images/iconmoi/27.png")} style={{height:15, width:15}}/>
                  <Text style={styles.textParent}>Thông tin người dùng</Text>
                </View>
              </TouchableOpacity>

              <View style={styles.menuChild}>
                <TouchableOpacity>
                  <View style={styles.boxChild}>
                    <Image source={require("../images/iconmoi/24.png")} style={styles.iconDot} />
                    <Text style={styles.textNoChild}>Hồ sơ cá nhân</Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity>
                  <View style={styles.boxChild}>
                    <Image source={require("../images/iconmoi/24.png")} style={styles.iconDot} />
                    <Text style={styles.textNoChild}>Lịch cá nhân</Text>
                  </View>
                </TouchableOpacity>
                <TouchableOpacity>
                  <View style={styles.boxChild}>
                    <Image source={require("../images/iconmoi/24.png")} style={styles.iconDot} />
                    <Text style={styles.textNoChild}>Ủy quyền</Text>
                  </View>
                </TouchableOpacity>

                <TouchableOpacity>
                  <View style={styles.boxChild}>
                    <Image source={require("../images/iconmoi/24.png")} style={styles.iconDot} />
                    <Text style={styles.textNoChild}>
                    Thoát
                    </Text>
                  </View>
                </TouchableOpacity>
                
              </View>
              {/*  */}
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    alignItems: "stretch"
  },
  boxInfoUser: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    borderBottomWidth: 2,
    borderBottomColor: "#005f16"
  },
  boxAvatar: {
    justifyContent: "center"
  },
  textChucVu: {
    color: COLOR_DARK_BLUE,
    fontSize: 9,
    fontFamily: "UTMAvo_1"
  },
  textName: {
    color: COLOR_DARK_BLUE,
    fontSize: 11,
    fontFamily: "UTMAvoBold_1",
    textTransform: "capitalize"
  },
  boxParent: {
    height: 34,
    flexDirection: "row",
    backgroundColor: "#e2f2f2",
    alignItems: "center",
    paddingLeft: 6
  },
  boxChild: {
    flexDirection: "row",
    height: 30,
    alignItems: "center"
    //   backgroundColor:'#eff4f4'
  },
  menuChild: {
    backgroundColor: "#eff4f4",
    paddingLeft: 30
  },
  textParent: {
    color: COLOR_BLACK,
    fontSize: 12,
    fontWeight: "bold",
    paddingLeft: 7
  },
  textChild: {
    color: COLOR_BLACK,
    fontSize: 12,
    paddingLeft: 7,
    fontWeight:'bold'
  },
  textNoChild:{
    color: COLOR_BLACK,
    fontSize: 12,
    paddingLeft: 7,
  },
  textChildSub:{
    color: COLOR_BLACK,
    fontSize: 12,
    paddingLeft: 13
  },
  subMenu: {
    backgroundColor: COLOR_WHITE
  },
  boxSubMenu:{
      borderLeftWidth:0.5,
      borderLeftColor:'#b3b3b3',
      marginLeft:33,
      marginVertical:3
  },
  iconDot:{
    width:8,
    height:8
  }
});
