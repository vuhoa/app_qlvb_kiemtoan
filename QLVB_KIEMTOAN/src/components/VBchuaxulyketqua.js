/*
This is Splash Screen, run first time when app start
*/
import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Animated,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  Image,
  ScrollView
} from "react-native";
import { Styles } from "../config/Styles";
import {
  COLOR_GREEN,
  COLOR_BLUE,
  COLOR_GRAY,
  COLOR_WHITE,
  COLOR_DARK_BLUE
} from "../config/MyColor";
import { Hoshi } from "react-native-textinput-effects";
import Ripple from "react-native-material-ripple";
// import { TouchableOpacity } from "react-native-gesture-handler";
import {
  Table,
  TableWrapper,
  Row,
  Rows,
  Col,
  Cols,
  Cell
} from "react-native-table-component";
export default class VBchuaxulyketqua extends Component<Props> {
  constructor(props) {
    super(props);
    this.state = {
      tableHead: [
        "Số đến",
        "Ngày đến",
        "Tên cơ quan/Tổ chức ban hành",
        "Số kí hiệu",
        "Ngày văn bản",
        "Trích yếu",
        "Hạn sử lý",
        "Đơn vị xử lý"
      ],
      widthArr:[50,85, 205, 90, 95, 328, 72, 184],
      tableData: [
        [
          "68",
          "05/06/2019",
          "BHXH VIETNAM",
          "116/TB-KTNN",
          "29/05/2019",
          "Quyết định số 479/QĐ-KTNN ngày 22/3/2019 của T ...",
          "",
          "Lãnh đạo xử lý: HÀ THỊ MỸ DUNG - Vụ trưởng"
        ],
        [
          "68",
          "05/06/2019",
          "BHXH VIETNAM",
          "116/TB-KTNN",
          "29/05/2019",
          "Quyết định số 479/QĐ-KTNN ngày 22/3/2019 của T ...",
          "",
          "Lãnh đạo xử lý: HÀ THỊ MỸ DUNG - Vụ trưởng"
        ],
        [
          "68",
          "05/06/2019",
          "BHXH VIETNAM",
          "116/TB-KTNN",
          "29/05/2019",
          "Quyết định số 479/QĐ-KTNN ngày 22/3/2019 của T ...0",
          "",
          "Lãnh đạo xử lý: HÀ THỊ MỸ DUNG - Vụ trưởng"
        ],
        [
          "68",
          "05/06/2019",
          "BHXH VIETNAM",
          "116/TB-KTNN",
          "29/05/2019",
          "Quyết định số 479/QĐ-KTNN ngày 22/3/2019 của T ...0",
          "",
          "Lãnh đạo xử lý: HÀ THỊ MỸ DUNG - Vụ trưởng"
        ],
        [
          "68",
          "05/06/2019",
          "BHXH VIETNAM",
          "116/TB-KTNN",
          "29/05/2019",
          "Quyết định số 479/QĐ-KTNN ngày 22/3/2019 của T ...0",
          "",
          "Lãnh đạo xử lý: HÀ THỊ MỸ DUNG - Vụ trưởng"
        ],
        [
          "68",
          "05/06/2019",
          "BHXH VIETNAM",
          "116/TB-KTNN",
          "29/05/2019",
          "Quyết định số 479/QĐ-KTNN ngày 22/3/2019 của T ...0",
          "",
          "Lãnh đạo xử lý: HÀ THỊ MỸ DUNG - Vụ trưởng"
        ],
        [
          "68",
          "05/06/2019",
          "BHXH VIETNAM",
          "116/TB-KTNN",
          "29/05/2019",
          "Quyết định số 479/QĐ-KTNN ngày 22/3/2019 của T ...0",
          "",
          "Lãnh đạo xử lý: HÀ THỊ MỸ DUNG - Vụ trưởng"
        ],
        [
          "68",
          "05/06/2019",
          "BHXH VIETNAM",
          "116/TB-KTNN",
          "29/05/2019",
          "Quyết định số 479/QĐ-KTNN ngày 22/3/2019 của T ...0",
          "",
          "Lãnh đạo xử lý: HÀ THỊ MỸ DUNG - Vụ trưởng"
        ],
        [
          "68",
          "05/06/2019",
          "BHXH VIETNAM",
          "116/TB-KTNN",
          "29/05/2019",
          "Quyết định số 479/QĐ-KTNN ngày 22/3/2019 của T ...0",
          "",
          "Lãnh đạo xử lý: HÀ THỊ MỸ DUNG - Vụ trưởng"
        ]
      ]
    };
  }

  static navigationOptions = ({ navigation }) => {
    let headerTitle = "Văn bản chưa xử lý";
    let headerTitleStyle = {
      textAlign: "center",
      flex: 1,
      alignSelf: "center",
      color: COLOR_WHITE,
      fontSize: 12,
      fontFamily: "UTMAvo_1",
      textTransform: "uppercase"
    };
    let headerStyle = {
      borderBottomWidth: 2,
      borderBottomColor: "#6fc3e9",
      height: 44
    };
    let headerLeft = (
      <TouchableOpacity
        onPress={() => {
          navigation.goBack();
        }}
      >
        <Image
          source={require("../images/iconmoi/39.png")}
          style={{ height: 20, width: 11, marginLeft: 15 }}
        />
      </TouchableOpacity>
    );
    let headerRight = (
      <View style={styles.boxSearch}>
        <TouchableOpacity
          onPress={() => {
            navigation.navigate("Home");
          }}
        >
         <Image
          source={require("../images/iconmoi/2.png")}
          style={{ height: 18, width: 15, marginRight: 15 }}
        />
        </TouchableOpacity>
      </View>
    );
    let headerBackground = (
      <Image
        source={require("../images/bg-header.png")}
        style={{ width: "100%", height: 42 }}
      />
    );
    return {
      headerBackground,
      headerTitle,
      headerStyle,
      headerLeft,
      headerTitleStyle,
      headerRight
    };
  };

  render() {
    return (
      <ImageBackground
        source={require("../images/iconmoi/71.png")}
        style={{ width: "100%", height: "100%", resizeMode: "contain" }}
      >
        <View style={styles.container}>
          <View style={{ flexDirection:'row'}}>
            <Image source={require('../images/iconmoi/Asset20.png')}  style={{ width: 18, height: 18 }}></Image>
            <Text style={styles.textTitle}>
             Kết quả tìm kiếm
            </Text>
          </View>
          <View style={styles.boxTable}>
            <ScrollView horizontal={true}>
              <View style={styles.insideTable}>
                <Table>
                  <Row
                   widthArr={this.state.widthArr}
                    data={this.state.tableHead}
                    style={styles.header}
                    textStyle={styles.textHead}
                  />
                </Table>
                <ScrollView style={styles.dataWrapper}>
                  <Table
                    borderStyle={{ borderWidth: 1, borderColor: "#e5e5e5" }}
                  >

                    <Rows
                    widthArr={this.state.widthArr}
                    style={styles.row}
                    data={this.state.tableData} textStyle={styles.text} />
                  </Table>
                </ScrollView>
              </View>
            </ScrollView>
          </View>
        </View>
      </ImageBackground>
    );
  }
}
const styles = StyleSheet.create({
  container:{
    flex:1,
    flexDirection:'column',
    margin:13
  },
  boxTable:{
    flex: 1, marginTop: 10, 
    backgroundColor: '#e1f8ff',
    borderRadius:6,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 5 },
    shadowOpacity: 0.2,
    shadowRadius: 5,
    elevation: 3
  },
  boxSearch: {
    flexDirection: "row"
  },
  textTitle:{
    fontSize:14,
    fontFamily: "UTMAvoBold_1",
    color:COLOR_BLUE,
    textTransform:'uppercase',
    paddingLeft:12
  },
  text:{
    color:'#000',
    fontSize:14,
    paddingLeft:7
  },
  header:{
    backgroundColor:COLOR_BLUE,
    height:35
  },
  textHead:{
    color:COLOR_WHITE,
    textAlign:'center'
  },
  dataWrapper:{
    backgroundColor:COLOR_WHITE
  },
  row:{
    height:70
  },
  insideTable:{
    margin:10
  }
});
